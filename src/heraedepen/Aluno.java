/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package heraedepen;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

/**
 *
 * @author Aluno
 */
public class Aluno extends Pessoa{
    protected String semestre;
    protected String curso;

    public String getSemestre() {
        return semestre;
    }

    public void setSemestre(String semestre) {
        this.semestre = semestre;
    }

    public String getCurso() {
        return curso;
    }

    public void setCurso(String curso) {
        this.curso = curso;
    }
    public void inserir(){
        Conexao conexao = new Conexao();
        Connection dbConnection = conexao.getConexao();
        
        PreparedStatement preparedStatement = null;
        
        String insertTableSQL = "insert into OO_cadastro(nome, idade, endereco, semestre, curso, funcao) values(?, ?, ?, ?, ?, ?)";
        
        try {
            preparedStatement = dbConnection.prepareStatement(insertTableSQL);
            
            preparedStatement.setString(1, nome);
            preparedStatement.setInt(2, idade);
            preparedStatement.setString(3, endereco);
            preparedStatement.setString(4, semestre);
            preparedStatement.setString(5, curso);
            preparedStatement.setString(6, funcao);
            preparedStatement.executeUpdate();
        } catch (Exception e) {
            e.printStackTrace();
        }
}

     public void update(){
        Conexao c = new Conexao();
        Connection dbConnection = c.getConexao();
        PreparedStatement ps;
        
        String updateTableSQL = "UPDATE oo_cadastro set idade = ?, endereco = ?, curso = ?, semestre = ?, funcao =?  where nome= ?";
        try{
            ps = dbConnection.prepareStatement(updateTableSQL);
            
            ps.setInt(1, this.idade);
            ps.setString(2, this.endereco);
            ps.setString(3, this.curso);
            ps.setString(4, this.semestre);
            ps.setString(5, this.funcao);
            ps.setString(6, this.nome);
            
            ps.executeUpdate();
        }catch(SQLException e){
            e.printStackTrace();
        }
    }

    

}
