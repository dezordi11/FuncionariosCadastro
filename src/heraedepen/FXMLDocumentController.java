/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package heraedepen;

import com.sun.javafx.scene.control.skin.TableViewSkinBase;
import java.net.URL;
import java.util.ArrayList;
import java.util.ResourceBundle;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.RadioButton;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.ToggleGroup;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.input.MouseDragEvent;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.AnchorPane;

/**
 *
 * @author Aluno
 */
public class FXMLDocumentController implements Initializable {
    
    @FXML
    private TableView<Pessoa> Tabela;
    @FXML
    private TableColumn<Pessoa, String> tbNome;
    @FXML
    private TableColumn<Pessoa, Integer> tbIdade;
    @FXML
    private TableColumn<Pessoa, String> tbEndereco;
    @FXML
    private TableColumn<Pessoa, String> tbFuncao;
    @FXML
    private RadioButton alunobot;
    @FXML
    private ToggleGroup Funcao;
    @FXML
    private RadioButton profbot;
    @FXML
    private TextField txNome;
    @FXML
    private TextField txIdade;
    @FXML
    private TextField txEndereco;
    @FXML
    private Label nmNome;
    @FXML
    private Label nmIdade;
    @FXML
    private Label nmEnderec;
    @FXML
    private Label nmSemestre;
    @FXML
    private Label nmCurso;
    @FXML
    private TextField txSemestre;
    @FXML
    private TextField txCurso;
    @FXML
    private TextField txFunc;
    @FXML
    private Label nmFunc;
    @FXML
    private TextField txDisc;
    @FXML
    private TextField txSal;
    @FXML
    private TextField txSet;
    @FXML
    private Label nmSetor;
    @FXML
    private Label nmDisc;
    @FXML
    private Label nmSal;
    @FXML
    private RadioButton funcbot;
    @FXML
    private AnchorPane apAlun;
    @FXML
    private AnchorPane apFuncAdm;
    @FXML
    private AnchorPane apProf;
    @FXML
    private TextField txSal1;
    @FXML
    private Label nmSal1;
    private ObservableList<Pessoa> pes;
    @FXML
    private Button botDetalhes;
    @FXML
    private Button botExcluir;
    @FXML
    private Button botEditar;
    @FXML
    private Label lbUm;
    @FXML
    private Label lbDois;
    @FXML
    private Label lbTres;
    private Pessoa det;
    @FXML
    private Label lbA;
    @FXML
    private Label lbB;
    @FXML
    private Label lbC;
    @FXML
    private Button botEditar1;
    @FXML
    private Button botCadastrar;
    
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        pes = Tabela.getItems();
          
        tbNome.setCellValueFactory(new PropertyValueFactory<>("nome"));      
        tbIdade.setCellValueFactory(new PropertyValueFactory<>("idade"));
        tbEndereco.setCellValueFactory(new PropertyValueFactory<>("endereco"));
        tbFuncao.setCellValueFactory(new PropertyValueFactory<>("funcao"));
        

        this.Tabela.setItems(pes);
        
        ArrayList<Pessoa> pesBanco = Pessoa.getAll();
        for(Pessoa pess: pesBanco){
            pes.add(pess);}
        
 /*           Aluno alUm = new Aluno();
            alUm.setNome("Gabriel");
            alUm.setIdade(16);
            alUm.setEndereco("Esperanca");
            alUm.setSemestre("Primeiro");
            alUm.setCurso("Informatica");
            alUm.setFuncao("Aluno");
            
            pes.add(alUm);
           
            
            
            Aluno alDois = new Aluno();
            alDois.setNome("Joao");
            alDois.setIdade(22);
            alDois.setEndereco("Esperanca");
            alDois.setSemestre("Segundo");
            alDois.setCurso("Publicidade");
            alDois.setFuncao("Aluno");
            
            pes.add(alDois);
            
            Aluno alTres = new Aluno();
            alTres.setNome("Beatriz");
            alTres.setIdade(17);
            alTres.setEndereco("Rio Branco");
            alTres.setSemestre("Primeiro");
            alTres.setCurso("Administração");
            alTres.setFuncao("Aluno");
            
            
            pes.add(alTres);
            
            FuncAdm funcUm = new FuncAdm();
            funcUm.setNome("Rodrigo");
            funcUm.setIdade(32);
            funcUm.setEndereco("Esperanca");
            funcUm.setSalario(200.00);
            funcUm.setSetor("Administrativo");
            funcUm.setFuncaoAdm("Orientador");
            funcUm.setFuncao("Funcionario");
            
            pes.add(funcUm);
            
            FuncAdm funcDois = new FuncAdm();
            funcDois.setNome("Calvett");
            funcDois.setIdade(31);
            funcDois.setEndereco("Rio Pardo");
            funcDois.setSalario(300.00);
            funcDois.setSetor("Administrativo");
            funcDois.setFuncaoAdm("Economista");
            funcDois.setFuncao("Funcionario");
            
            pes.add(funcDois);
            
            FuncAdm funcTres = new FuncAdm();
            funcTres.setNome("Lais");
            funcTres.setIdade(25);
            funcTres.setEndereco("Esperanca");
            funcTres.setSalario(600.00);
            funcTres.setSetor("TI");
            funcTres.setFuncaoAdm("Tecnico");
            funcTres.setFuncao("Funcionario");
            
            pes.add(funcTres);            
        

            Professor profUm = new Professor();
            profUm.setNome("Roberta");
            profUm.setIdade(28);
            profUm.setEndereco("Fatima");
            profUm.setSalario(100.00);
            profUm.setDisciplina("Administracao");
            profUm.setFuncao("Professor");
            
            pes.add(profUm);            

            Professor profDois = new Professor();
            profDois.setNome("Romury");
            profDois.setIdade(22);
            profDois.setEndereco("Niteroi");
            profDois.setSalario(400.00);
            profDois.setDisciplina("Psicologia");
            profDois.setFuncao("Professor");
            
            pes.add(profDois);             

            Professor profTres = new Professor();
            profTres.setNome("Felipe");
            profTres.setIdade(24);
            profTres.setEndereco("Roraima");
            profTres.setSalario(50.00);
            profTres.setDisciplina("Filosofia");
            profTres.setFuncao("Professor");
            
            pes.add(profTres); 
  */          
    }


   
    @FXML
    private void Cadastrar(ActionEvent event) {
    lbA.setVisible(false);
    lbB.setVisible(false);
    lbUm.setVisible(false);
    lbDois.setVisible(false);
    lbC.setVisible(false);
    lbTres.setVisible(false);
        if (alunobot.isSelected()){
            Aluno al = new Aluno();
            al.setNome(txNome.getText());
            al.setIdade(Integer.parseInt(txIdade.getText()));
            al.setEndereco(txEndereco.getText());
            al.setSemestre(txSemestre.getText());
            al.setCurso(txCurso.getText());
            al.setFuncao("Aluno");
            
            pes.add(al);
            al.inserir();
        }
        else if (funcbot.isSelected()){
            FuncAdm adm = new FuncAdm();
            adm.setNome(txNome.getText());
            adm.setIdade(Integer.parseInt(txIdade.getText()));
            adm.setEndereco(txEndereco.getText());
            adm.setSalario(Double.parseDouble(txSal.getText()));
            adm.setSetor(txSet.getText());
            adm.setFuncaoAdm(txFunc.getText());
            adm.setFuncao("Funcionario");
            
            pes.add(adm);
            adm.inserir();
        }
        else if (profbot.isSelected()){
            Professor prof = new Professor();
            prof.setNome(txNome.getText());
            prof.setIdade(Integer.parseInt(txIdade.getText()));
            prof.setEndereco(txEndereco.getText());
            prof.setSalario(Double.parseDouble(txSal1.getText()));
            prof.setDisciplina(txDisc.getText());
            prof.setFuncao("Professor");
            
            pes.add(prof);  
            prof.inserir();
        }        
    }

    
    @FXML
    private void alunobot(ActionEvent event) {
        if(alunobot.isSelected()){
            apAlun.setVisible(true);
            apProf.setVisible(false);
            apFuncAdm.setVisible(false);
            apAlun.setDisable(false);
            apProf.setDisable(true);
            apFuncAdm.setDisable(true);
        }
    }

    @FXML
    private void profbot(ActionEvent event) {
            if(profbot.isSelected()){
            apAlun.setVisible(false);
            apProf.setVisible(true);
            apFuncAdm.setVisible(false);
            apAlun.setDisable(true);
            apProf.setDisable(false);
            apFuncAdm.setDisable(true);
        }
    }

    @FXML
    private void funcbot(ActionEvent event) {
            if(funcbot.isSelected()){
            apAlun.setVisible(false);
            apProf.setVisible(false);
            apFuncAdm.setVisible(true);
            apAlun.setDisable(true);
            apProf.setDisable(true);
            apFuncAdm.setDisable(false);
        }
    }

    @FXML
    private void liberarBot(MouseEvent event) {
    botDetalhes.setDisable(false);
    botExcluir.setDisable(false);
    botEditar.setDisable(false);
    }

    @FXML
    private void botDetalhes(ActionEvent event) {
        det = Tabela.getSelectionModel().getSelectedItem();
        lbA.setVisible(true);
        lbB.setVisible(true);
        lbUm.setVisible(true);
        lbDois.setVisible(true);
        if ("Funcionario".equals(det.getFuncao())){
        lbC.setVisible(true);
        lbTres.setVisible(true);
        
        FuncAdm aux = (FuncAdm) det;
        lbA.setText("Salario:");
        lbB.setText("Setor:");
        lbUm.setText(String.valueOf(aux.getSalario()));
        lbDois.setText(aux.getSetor());
        lbTres.setText(aux.getFuncaoAdm());
        } 
        if ("Aluno".equals(det.getFuncao())){
        lbC.setVisible(false);
        lbTres.setVisible(false);
        Aluno aux = (Aluno) det;
        lbA.setText("Semestre:");
        lbB.setText("Curso:");
        lbUm.setText(aux.getSemestre() );
        lbDois.setText(aux.getCurso());
        } 
        if ("Professor".equals(det.getFuncao())){
        lbC.setVisible(false);
        lbTres.setVisible(false);
        Professor aux = (Professor) det;
        lbA.setText("Salario:");
        lbB.setText("Disciplina:");
        lbUm.setText(String.valueOf(aux.getSalario()));
        lbDois.setText(aux.getDisciplina());
        } 
        
    }

    @FXML
    private void botExcluir(ActionEvent event) {
    lbA.setVisible(false);
    lbB.setVisible(false);
    lbUm.setVisible(false);
    lbDois.setVisible(false);
    lbC.setVisible(false);
    lbTres.setVisible(false);
        Tabela.getSelectionModel().getSelectedItem().delete();
        pes.remove(Tabela.getSelectionModel().getSelectedItem());

    }

    @FXML
    private void botEditar(ActionEvent event) {
    lbA.setVisible(false);
    lbB.setVisible(false);
    lbUm.setVisible(false);
    lbDois.setVisible(false);
    lbC.setVisible(false);
    lbTres.setVisible(false);
    det = Tabela.getSelectionModel().getSelectedItem();
    botEditar1.setVisible(true);
    botEditar1.setDisable(false);
    botCadastrar.setVisible(false);
    botCadastrar.setDisable(true);
    botEditar.setDisable(true);
    botExcluir.setDisable(true);
    
    if ("Funcionario".equals(det.getFuncao())){
        funcbot.setSelected(true);
        funcbot(event);
        FuncAdm aux = (FuncAdm) det;
        txNome.setText(aux.getNome());
        txIdade.setText(String.valueOf(aux.getIdade()));
        txEndereco.setText(aux.getEndereco());
        txSal.setText(String.valueOf(aux.getSalario()));
        txSet.setText(aux.getSetor());
        txFunc.setText(aux.getFuncao());
    }
        if ("Aluno".equals(det.getFuncao())){
        alunobot.setSelected(true);
        alunobot(event);
        Aluno aux = (Aluno) det;
        txNome.setText(aux.getNome());
        txIdade.setText(String.valueOf(aux.getIdade()));
        txEndereco.setText(aux.getEndereco());
        txSemestre.setText(aux.getSemestre());
    txCurso.setText(aux.getCurso());
           
    }
            if ("Professor".equals(det.getFuncao())){

        profbot.setSelected(true);
        profbot(event);
        Professor aux = (Professor) det;
        txNome.setText(aux.getNome());
        txIdade.setText(String.valueOf(aux.getIdade()));
        txEndereco.setText(aux.getEndereco());
        txSal1.setText(String.valueOf(aux.getSalario()));
        txDisc.setText(aux.getDisciplina());
    }
    
    }

    @FXML
    private void deselect(MouseEvent event) {
    botDetalhes.setDisable(true);
    botExcluir.setDisable(true);
    botEditar.setDisable(true);
    lbA.setVisible(false);
    lbB.setVisible(false);
    lbUm.setVisible(false);
    lbDois.setVisible(false);
    lbC.setVisible(false);
    lbTres.setVisible(false);
    txNome.setText("");
    txIdade.setText("");
    txEndereco.setText("");
    txSemestre.setText("");
    txCurso.setText("");
    txSal.setText("");
    txSal1.setText("");
    txDisc.setText("");
    txSet.setText("");
    txFunc.setText("");
    botEditar1.setVisible(false);
    botEditar1.setDisable(true);
    botCadastrar.setDisable(false);
    botCadastrar.setVisible(true);
/*    alunobot.setSelected(false);
    funcbot.setSelected(false);
    profbot.setSelected(false);
    txSemestre.setVisible(false);
    txCurso.setVisible(false);
    txSal.setVisible(false);
    txDisc.setVisible(false);
    txSet.setVisible(false);
    txFunc.setVisible(false);    
*/

    }

    @FXML
    private void botEditar1(ActionEvent event) {
        det = Tabela.getSelectionModel().getSelectedItem();
            if (alunobot.isSelected()){
            Aluno det1 = new Aluno();
            det1.setNome(det.getNome());
            det1.setIdade(Integer.parseInt(txIdade.getText()));
            det1.setEndereco(txEndereco.getText());
            det1.setSemestre(txSemestre.getText());
            det1.setCurso(txCurso.getText());
            det1.setFuncao("Aluno");
            det1.update();
            }
        else if (funcbot.isSelected()){
            FuncAdm det1 = new FuncAdm();
            det1.setNome(det.getNome());
            det1.setIdade(Integer.parseInt(txIdade.getText()));
            det1.setEndereco(txEndereco.getText());
            det1.setSalario(Double.parseDouble(txSal.getText()));
            det1.setSetor(txSet.getText());
            det1.setFuncaoAdm(txFunc.getText());
            det1.setFuncao("Funcionario");
            det1.update();
              
        }
        else if (profbot.isSelected()){
            Professor det1 = new Professor();
            det1.setNome(det.getNome());
            det1.setIdade(Integer.parseInt(txIdade.getText()));
            det1.setEndereco(txEndereco.getText());
            det1.setSalario(Double.parseDouble(txSal1.getText()));
            det1.setDisciplina(txDisc.getText());
            det1.setFuncao("Professor");
            det1.update();
            
        }       
                    pes = Tabela.getItems();
                    pes.clear();
        tbNome.setCellValueFactory(new PropertyValueFactory<>("nome"));      
        tbIdade.setCellValueFactory(new PropertyValueFactory<>("idade"));
        tbEndereco.setCellValueFactory(new PropertyValueFactory<>("endereco"));
        tbFuncao.setCellValueFactory(new PropertyValueFactory<>("funcao"));
        

        this.Tabela.setItems(pes);
       ArrayList<Pessoa> pesBanco = Pessoa.getAll();
       for(Pessoa pess: pesBanco){
            pes.add(pess);}
        
    lbA.setVisible(false);
    lbB.setVisible(false);
    lbUm.setVisible(false);
    lbDois.setVisible(false);
    lbC.setVisible(false);
    lbTres.setVisible(false);
    botCadastrar.setVisible(true);
    botCadastrar.setDisable(false);
    botEditar1.setVisible(false);
    botEditar1.setDisable(true);
    Tabela.refresh();  

    }

}
