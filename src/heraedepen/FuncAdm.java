/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package heraedepen;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;

/**
 *
 * @author Aluno
 */
public class FuncAdm extends Funcionario{
    protected String setor;
    protected String funcaoadm;

    public String getSetor() {
        return setor;
    }

    public void setSetor(String setor) {
        this.setor = setor;
    }

    public String getFuncaoAdm() {
        return funcaoadm;
    }

    public void setFuncaoAdm(String funcaoadm) {
        this.funcaoadm = funcaoadm;
    }
    
    public void inserir(){
        Conexao conexao = new Conexao();
        Connection dbConnection = conexao.getConexao();
        
        PreparedStatement preparedStatement = null;
        
        String insertTableSQL = "insert into OO_cadastro(nome, idade, endereco, salario, setor, funcaoadm, funcao) values(?, ?, ?, ?, ?, ?, ?)";
        
        try {
            preparedStatement = dbConnection.prepareStatement(insertTableSQL);
            
            preparedStatement.setString(1, nome);
            preparedStatement.setInt(2, idade);
            preparedStatement.setString(3, endereco);
            preparedStatement.setDouble(4, salario);
            preparedStatement.setString(5, setor);
            preparedStatement.setString(6, funcaoadm);
            preparedStatement.setString(7, funcao);
            preparedStatement.executeUpdate();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
     public void update(){
        Conexao c = new Conexao();
        Connection dbConnection = c.getConexao();
        PreparedStatement ps;
        
        String updateTableSQL = "UPDATE oo_cadastro set idade = ?, endereco = ?, salario = ?, setor = ?, funcaoadm =?, funcao =?  where nome= ?";
        try{
            ps = dbConnection.prepareStatement(updateTableSQL);
            
            ps.setInt(1, this.idade);
            ps.setString(2, this.endereco);
            ps.setDouble(3, this.salario);
            ps.setString(4, this.setor);
            ps.setString(5, this.funcaoadm);
            ps.setString(6, this.funcao);
            ps.setString(7, this.nome);
            
            ps.executeUpdate();
        }catch(SQLException e){
            e.printStackTrace();
        }
    }
}
