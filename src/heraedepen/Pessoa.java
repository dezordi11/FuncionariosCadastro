/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package heraedepen;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

/**
 *
 * @author Aluno
 */
public class Pessoa {
    protected String nome;
    protected int idade;
    protected String endereco;
    protected String funcao;

        public static ArrayList<Pessoa> getAll(){
        String selectSQL = "Select * from oo_cadastro";
        ArrayList<Pessoa> pes = new ArrayList<>();
        
        Conexao conexao = new Conexao();
        Connection dbConnection = conexao.getConexao();
        
        Statement statement;
        
        try {
            statement = dbConnection.createStatement();
            ResultSet resultado = statement.executeQuery(selectSQL);
            
            while (resultado.next()) {
                if(resultado.getString("funcao").equals("Aluno")){
                Aluno pe = new Aluno();
                pe.setNome(resultado.getString("nome"));
                pe.setIdade(resultado.getInt("idade"));
                pe.setEndereco(resultado.getString("endereco"));
                pe.setFuncao(resultado.getString("funcao"));
                pe.setCurso(resultado.getString("curso"));
                pe.setSemestre(resultado.getString("semestre"));
                pes.add(pe);
            }
                else if (resultado.getString("funcao").equals("Professor")){
                Professor pe = new Professor();
                pe.setNome(resultado.getString("nome"));
                pe.setIdade(resultado.getInt("idade"));
                pe.setEndereco(resultado.getString("endereco"));
                pe.setFuncao(resultado.getString("funcao"));
                pe.setDisciplina(resultado.getString("disciplina"));
                pe.setSalario(resultado.getDouble("salario"));
                
                pes.add(pe);
            }
                if(resultado.getString("funcao").equals("Funcionario")){
                FuncAdm pe = new FuncAdm();
                pe.setNome(resultado.getString("nome"));
                pe.setIdade(resultado.getInt("idade"));
                pe.setEndereco(resultado.getString("endereco"));
                pe.setFuncaoAdm(resultado.getString("funcaoadm"));
                pe.setSalario(resultado.getDouble("salario"));
                pe.setSetor(resultado.getString("setor"));
                pe.setFuncao(resultado.getString("funcao"));                
                pes.add(pe);
            }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return pes;
    }
         public boolean delete(){
     Conexao c = new Conexao();
     Connection dbConnection = c.getConexao();
     PreparedStatement ps= null;
     String insertTableSQL = "DELETE FROM oo_cadastro WHERE nome = ?";
     try{
        ps = dbConnection.prepareStatement(insertTableSQL);
        ps.setString(1, this.nome);
        ps.executeUpdate();
     } catch (SQLException e){
        e.printStackTrace();}
     finally{ c.desconecta();}
     return true;
     }    
        
    public String getFuncao() {
        return funcao;
    }

    public void setFuncao(String funcao) {
        this.funcao = funcao;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public int getIdade() {
        return idade;
    }

    public void setIdade(int idade) {
        this.idade = idade;
    }

    public String getEndereco() {
        return endereco;
    }

    public void setEndereco(String endereco) {
        this.endereco = endereco;
    }
    
}
