/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package heraedepen;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;

/**
 *
 * @author Aluno
 */
public class Professor extends Funcionario{
    protected String disciplina;

    public String getDisciplina() {
        return disciplina;
    }

    public void setDisciplina(String disciplina) {
        this.disciplina = disciplina;
    }
       
    public void inserir(){
        Conexao conexao = new Conexao();
        Connection dbConnection = conexao.getConexao();
        
        PreparedStatement preparedStatement = null;
        
        String insertTableSQL = "insert into OO_cadastro(nome, idade, endereco, salario, disciplina, funcao) values(?, ?, ?, ?, ?, ?)";
        
        try {
            preparedStatement = dbConnection.prepareStatement(insertTableSQL);
            
            preparedStatement.setString(1, nome);
            preparedStatement.setInt(2, idade);
            preparedStatement.setString(3, endereco);
            preparedStatement.setDouble(4, salario);
            preparedStatement.setString(5, disciplina);
            preparedStatement.setString(6, funcao);            
            
            preparedStatement.executeUpdate();
        } catch (Exception e) {
            e.printStackTrace();
        }
}
         public void update(){
        Conexao c = new Conexao();
        Connection dbConnection = c.getConexao();
        PreparedStatement ps;
        
        String updateTableSQL = "UPDATE oo_cadastro set idade = ?, endereco = ?, salario = ?, disciplina = ?, funcao = ?  where nome= ?";
        try{
            ps = dbConnection.prepareStatement(updateTableSQL);
            
            ps.setInt(1, this.idade);
            ps.setString(2, this.endereco);
            ps.setDouble(3, this.salario);
            ps.setString(4, this.disciplina);
            ps.setString(5, this.funcao);
            ps.setString(6, this.nome);
            
            ps.executeUpdate();
        }catch(SQLException e){
            e.printStackTrace();
        }
    }
}
